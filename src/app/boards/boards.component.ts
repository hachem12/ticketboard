import { Component, OnInit} from '@angular/core';
import { Board } from '../shared/board.model';


@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.css']
})
export class BoardsComponent implements OnInit { 
  wasBoardSelected: boolean = false;
  boardChosen: Board;
  displayDialog: boolean = false;
  color: string;

  boards: Board[] = [
    new Board('first project', '#42cbf4'),
    new Board('second project', '#95f441')
  ]

  constructor() { }

  ngOnInit() {
  }

  showDialog() {
    this.displayDialog = true;
  }

  onColorPick(e) {
    this.color = e.value;
  }

  onConfirm(nameInput: HTMLInputElement) {
    this.boards.push(new Board(nameInput.value,this.color));
    this.displayDialog = false;
  }

  onBoardSelect(board : Board) {
    this.wasBoardSelected = true;
    this.boardChosen = board;
  }

}
