export class Model {
    constructor(
        public title: string, 
        public type: string, 
        public priority: string, 
        public createdDate: string, 
        public endDate: string, 
        public status: string, 
        public info: string
        ) {}
}