import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {ButtonModule} from 'primeng/button';
import {DialogModule} from 'primeng/dialog';
import {ColorPickerModule} from 'primeng/colorpicker';


import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { BoardsComponent } from './boards/boards.component';
import { StatsComponent } from './stats/stats.component';
import { CreateBoardDialogComponent } from './boards/create-board-dialog/create-board-dialog.component';
import { BoardComponent } from './boards/board/board.component';
import { HeaderComponent } from './boards/board/header/header.component';
import { BodyComponent } from './boards/board/body/body.component';
import { TicketComponent } from './boards/board/ticket/ticket.component';
import { TicketDialogComponent } from './boards/board/ticket-dialog/ticket-dialog.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BoardsComponent,
    StatsComponent,
    CreateBoardDialogComponent,
    BoardComponent,
    HeaderComponent,
    BodyComponent,
    TicketComponent,
    TicketDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    RouterModule,
    ButtonModule,
    DialogModule,
    ColorPickerModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
